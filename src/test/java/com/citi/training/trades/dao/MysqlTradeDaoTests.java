package com.citi.training.trades.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trades.exceptions.TradeNotFoundException;
import com.citi.training.trades.model.Trade;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlTradeDaoTests {
	
	@Autowired
	MysqlTradeDao mysqlTradeDao;

	@Test
	@Transactional
	public void test_createAndFindAll() {
		mysqlTradeDao.create(new Trade(-1, 11, 10.0, "GOOGL"));

		assertEquals(mysqlTradeDao.findAll().size(), 1);
	}

	@Test
	@Transactional
	public void test_findById() {
		Trade trade = mysqlTradeDao.create(new Trade(-1, 11, 10.0, "GOOGL"));

		assertEquals(mysqlTradeDao.findById(trade.getId()).getStockSymbol(), "GOOGL");
		assert (mysqlTradeDao.findById(trade.getId()).getDollarAmount() == 10.0);
		assertEquals(mysqlTradeDao.findById(trade.getId()).getNumberStocks(), 11);
	}
	
	@Test
	@Transactional
	public void test_deleteById() {
		Trade trade = mysqlTradeDao.create(new Trade(-1, 11, 10.0, "GOOGL"));

		mysqlTradeDao.deleteById(trade.getId());
		
		assertEquals(mysqlTradeDao.findAll().size(), 0);
	}
	
	@Test(expected = TradeNotFoundException.class)
	@Transactional
	public void test_TradeNotFound()
	{
		mysqlTradeDao.findById(1);
	}

}
