package com.citi.training.trades.dao;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.citi.training.trades.model.Trade;

public class InmemTradeDaoTests {
	
	private static Logger LOG = LoggerFactory.getLogger(InmemTradeDaoTests.class);

    private String testStock = "GOOGL";
    private double testPrice = 123.99;
    private int testVolume = 12;

    @Test
    public void test_saveAndGetTrade() {
        Trade testTrade = new Trade(testVolume, testPrice, testStock);
        InmemTradeDao testRepo = new InmemTradeDao();

        testTrade = testRepo.create(testTrade);
 
        assertTrue(testRepo.findById(testTrade.getId()).equals(testTrade));

    }

    @Test
    public void test_saveAndGetAllTrades() {
        Trade[] testTradeArray = new Trade[100];
        InmemTradeDao testRepo = new InmemTradeDao();

        for(int i=0; i<testTradeArray.length; ++i) {
            testTradeArray[i] = new Trade(testVolume, testPrice, testStock);

            testRepo.create(testTradeArray[i]);
        }

        List<Trade> returnedTrades = testRepo.findAll();
        LOG.info("Received [" + returnedTrades.size() +
                 "] Trades from repository");

        for(Trade thisTrade: testTradeArray) {
            assertTrue(returnedTrades.contains(thisTrade));
        }
        LOG.info("Matched [" + testTradeArray.length + "] Trades");
    }

}
