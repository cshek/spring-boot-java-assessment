package com.citi.training.trades.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TradeTests {
	
	private int testNumStocks = 10;
	private String testSymbol = "GOOGL";
	private double testDollarAmount = 104.0;
	
	 @Test
	    public void test_Trade_constructor() {
	        Trade testTrade = new Trade(testNumStocks, testDollarAmount, testSymbol);

	        assertEquals(testNumStocks, testTrade.getNumberStocks());
	        assertEquals(testSymbol, testTrade.getStockSymbol());
	        assertEquals(testDollarAmount, testTrade.getDollarAmount(), 104.0);
	    }

	    @Test
	    public void test_Trade_toString() {
	        String testString = new Trade(testNumStocks, testDollarAmount, testSymbol).toString();

	        assertTrue(testString.contains((new Integer(testNumStocks)).toString()));
	        assertTrue(testString.contains(testSymbol));
	        assertTrue(testString.contains(String.valueOf(testDollarAmount)));
	    }

}
