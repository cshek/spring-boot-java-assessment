package com.citi.training.trades.exceptions;

/**
 * Custom exception class to handle Trade not found exceptions
 * 
 * @author Administrator
 *
 */
public class TradeNotFoundException extends RuntimeException {
	
	public TradeNotFoundException(String msg)
	{
		super(msg);
	}

}
