package com.citi.training.trades.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trades.dao.TradeDao;
import com.citi.training.trades.model.Trade;

/**
 * Service class used to make calls to the DAO classes.
 * 
 * @author Administrator
 *
 */
@Component
public class TradeService {
	
	@Autowired
	private TradeDao tradeDao;
	
	public List<Trade> findAll()
	{
		return tradeDao.findAll();
	}

	public Trade findById(int id)
    {
    	return tradeDao.findById(id);
    }

	public Trade create(Trade trade)
    {
		if(trade.getStockSymbol().length() > 0)
		{
			return tradeDao.create(trade);
		}
		throw new RuntimeException("Invalid Parameter: stock name: " + trade.getStockSymbol());
    }

	public void deleteById(int id) {
    	tradeDao.deleteById(id);
    }

}
