package com.citi.training.trades.model;

/**
 * Model trade class to create trade objects.
 * 
 * @author Administrator
 *
 */
public class Trade {

	private int numberStocks;
	private String stockSymbol;
	private double dollarAmount;
	private int id;
	
	public Trade()
	{
		
	}

	public Trade(int numStocks, double dollarAmount, String stockSymbol)
	{
		this(-1, numStocks, dollarAmount, stockSymbol);
	}
	
	public Trade(int id, int numStocks, double dollarAmount, String stockSymbol) {
		this.numberStocks = numStocks;
		this.stockSymbol = stockSymbol;
		this.dollarAmount = dollarAmount;
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNumberStocks() {
		return numberStocks;
	}

	public void setNumberStocks(int numberStocks) {
		this.numberStocks = numberStocks;
	}

	public String getStockSymbol() {
		return stockSymbol;
	}

	public void setStockSymbol(String stockSymbol) {
		this.stockSymbol = stockSymbol;
	}

	public double getDollarAmount() {
		return dollarAmount;
	}

	public void setDollarAmount(double dollarAmount) {
		this.dollarAmount = dollarAmount;
	}

	@Override
	public String toString() {
		return "Trade [numberStocks=" + numberStocks + ", stockSymbol=" + stockSymbol + ", dollarAmount=" + dollarAmount
				+ "]";
	}

}
