package com.citi.training.trades.dao;

import java.util.List;

import com.citi.training.trades.model.Trade;

public interface TradeDao {
	
	Trade create(Trade trade);
	
	Trade findById(int id);
	
	void deleteById(int id);
	
	List<Trade> findAll();

}
