package com.citi.training.trades.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.citi.training.trades.exceptions.TradeNotFoundException;
import com.citi.training.trades.model.Trade;

/**
 * Provides an interface to provide access to the h2 memory configuration.
 * 
 * @author Administrator
 *
 */
@Component
@Profile("inmem-dao")
public class InmemTradeDao implements TradeDao{
	
	private static AtomicInteger idGenerator = new AtomicInteger();

    private Map<Integer, Trade> allTrades = new HashMap<Integer, Trade>();

    /**
     * Creates a {@link com.citi.training.trades.model.Trade} object and adds to the the Trade HashMap.
     * 
     * @param trade The new {@link com.citi.training.trades.model.Trade} object being added to the HashMap.
     */
    @Override
    public Trade create(Trade trade) {
        trade.setId(idGenerator.addAndGet(1));
        allTrades.put(trade.getId(), trade);
        return trade;
    }

    /**
     * Finds a {@link com.citi.training.trades.model.Trade} object using its integer id.
     * 
     * @param id The id of the object being searched for.
     * @return The {@link com.citi.training.trades.model.Trade} object that was found or HTTP 404.
     */
    @Override
    public Trade findById(int id) {
        Trade trade = allTrades.get(id);
        if (trade == null) {
            throw new TradeNotFoundException("Trade has not been found");
        }
        return trade;
    }

    /**
     * Returns a list of all the {@link com.citi.training.trades.model.Trade} objects stored in the HashMap.
     * 
     * @return The list of {@link com.citi.training.trades.model.Trade} objects in the HashMap.
     */
    @Override
    public List<Trade> findAll() {
        return new ArrayList<Trade>(allTrades.values());
    }

    /**
     * Deletes a {@link com.citi.training.trades.model.Trade} object using its id.
     * 
     * @param id The integer id of the {@link com.citi.training.trades.model.Trade} object to be deleted.
     */
    @Override
    public void deleteById(int id) {
       Trade trade = allTrades.remove(id);
       if (trade == null) {
           throw new TradeNotFoundException("Trade could not be deleted");
       }
    }

}
