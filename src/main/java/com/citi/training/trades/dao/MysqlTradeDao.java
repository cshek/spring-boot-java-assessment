package com.citi.training.trades.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trades.exceptions.TradeNotFoundException;
import com.citi.training.trades.model.Trade;

/**
 * DAO interface to provide access to the MYSQL database
 * 
 * @author Administrator
 */
@Component
public class MysqlTradeDao implements TradeDao {

	@Autowired
	JdbcTemplate tpl;

	/**
	 * Calls an SQL query that returns all the employee records held in the
	 * database.
	 * 
	 * @return Returns a list of {@link com.citi.training.trades.model.Trade}
	 *         objects.
	 */
	public List<Trade> findAll() {
		return tpl.query("SELECT id, stock, price, volume FROM trade", new TradeMapper());
	}

	/**
	 * Deletes an {@link com.citi.training.trades.model.Trade} object from the
	 * database using its id.
	 * 
	 * @param id The id of the employee to delete
	 */
	public void deleteById(int id) {
		findById(id);
		tpl.update("DELETE FROM trade WHERE id=?", id);
	}

	/**
	 * Finds an {@link com.citi.training.trades.model.Trade} in the database using
	 * its id.
	 * 
	 * @param id The id of the employee to find
	 * @return The {@link com.citi.training.trades.model.Trade} object that was found
	 *         or HTTP 404.
	 */
	public Trade findById(int id) {
		List<Trade> trades = tpl.query("SELECT id, stock, price, volume FROM trade WHERE id=?", new Object[] { id },
				new TradeMapper());

		if (trades.size() <= 0) {
			throw new TradeNotFoundException("Trade with id=[" + id + "] not found");
		}

		return trades.get(0);

	}

	/**
	 * Creates an {@link com.citi.training.trades.model.Trade} object and stores the
	 * object in a database.
	 * 
	 * @param trade An {@link com.citi.training.trades.model.Trade} object to be
	 *                 added to the database.
	 * @return The created {@link com.citi.training.trades.model.Trade} object.
	 */
	public Trade create(Trade trade) {
		KeyHolder keyHolder = new GeneratedKeyHolder();

		tpl.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

				PreparedStatement ps = connection.prepareStatement(
						"insert into trade (stock, price, volume) values (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, trade.getStockSymbol());
				ps.setDouble(2, trade.getDollarAmount());
				ps.setInt(3, trade.getNumberStocks());
				return ps;

			}
		}, keyHolder);
		trade.setId(keyHolder.getKey().intValue());
		return trade;
	}

	/**
	 * Mapper class that maps the {@link com.citi.training.trades.model.Trade} to a
	 * database row.
	 * 
	 * @author Administrator
	 */
	private static final class TradeMapper implements RowMapper<Trade> {

		/**
		 * Maps {@link com.citi.training.trades.model.Trade} object to a database row.
		 */
		public Trade mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Trade(rs.getInt("id"), rs.getInt("volume"), rs.getDouble("price"), rs.getString("stock"));
		}

	}

}
